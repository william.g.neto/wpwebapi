from webwhatsapi import WHatsAPIDriver

d = WhatsAPIDriver()

chats = d.get_all_chats()
bans = []
for c in chats:
    messages_ids = d.get_all_message_ids_in_chat(c, include_me=True)
    message = d.get_message_by_id(messages_ids[len(messages_ids)-1])
    if not isinstance(message, bool):
        status = message._js_obj["ack"]
        if message.sender.get_safe_name() == "You":
            if status == 1:
                bans.append(c.id)
print("%s chats deletaveis encontrados" % str(len(bans)))
for i in bans:
    d.delete_chat(i)
    print("Deletando...")
