CREATE DATABASE wpwebapi_testes;
USE wpwebapi_testes;

CREATE TABLE api_users (
	id  MEDIUMINT NOT NULL AUTO_INCREMENT,
	name varchar(100),
	number varchar(14),
	hash varchar(300),
	PRIMARY KEY (id)
);