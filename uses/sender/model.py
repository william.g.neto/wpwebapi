import mysql.connector

CONFIG = {
  'user': 'root',
  'password': 'will242697',
  'host': '127.0.0.1',
  'database': 'sender',
  'raise_on_warnings': True,
}

class WPSenderModel():
	def __init__(self):
		self.connection = mysql.connector.connect(**CONFIG)
		self.cursor = self.connection.cursor()

	def _close(self):
		self.connection.close()

	def add(self, num):
		if self.get(num) == False:
			sql = ("INSERT INTO envios (numero, status) VALUES (%s, %s)")
			self.cursor.execute(sql, (num, "0"))

			self.connection.commit()

	def get(self, num):
		try:
			sql = ("SELECT numero, status FROM envios WHERE numero = %s")
			self.cursor.execute(sql, (num, ))

			obj = False
			for (numero, status) in self.cursor:
				obj = {
					"numero": numero,
					"status": status
				}

			return obj
		except Exception as e:
			raise False
	
	def change_status(self, num):
		sql = ("UPDATE envios SET status = %s WHERE numero = %s")
		self.cursor.execute(sql, ("1", num))

		self.connection.commit()

	