import sys
import csv
from sender import WPSender

class SlotsManager():
	def __init__(self, num):
		self.chips = []
		self.chips_name = "chip"
		self.num = int(num)

		self.numbers_file = "inputs/numbers.csv"
		self.numbers = self._get_numbers()

	def _get_numbers(self):
		numbers = []		
		with open(self.numbers_file, encoding='utf-8') as csvfile:
			csvreader = csv.reader(csvfile)

			for row in csvreader:
				numbers = [x for x in row if x]

		self.numbers = numbers		
		return numbers

	def start_slot(self):
		print ("A carregar %s chips" % self.num)
		res = []
		i = 1
		total = len(self.numbers)
		parts = int(total/self.num)
		while i <= self.num:
			chip_name = "%s%s" % (self.chips_name, str(i))
			print("%s) Iniciando..." % chip_name)

			sender = WPSender(chip_name)
			check = sender.check_login()
			if check["status"] == 2:
				slice = self.numbers[:parts]
				obj = {
					"chip": chip_name,
					"status": "chip iniciado",
					"numeros": slice
				}
				res.append(obj)
				self.chips.append(obj)
				print("%s) chip iniciado" % chip_name)

				for sl in slice:
					self.numbers.remove(sl)
			elif check["status"] == 4:
				sender.login()
				response = sender.check_login()

				if response["status"] == 2:	
					slice = self.numbers[:parts]
					obj = {
						"chip": chip_name,
						"status": "chip iniciado",
						"numeros": slice
					}
					res.append(obj)
					self.chips.append(obj)
					print("%s) Slot iniciado" % chip_name)

					for sl in slice:
						self.numbers.remove(sl)
			else:
				res.append({
					"slot": chip_name,
					"status": "impossível iniciar",
				})
				print("%s) Falha ao iniciar" % chip_name)
			i += 1

if __name__ == "__main__":
	sm = SlotsManager(sys.argv[1])
	sm.start_slot()





