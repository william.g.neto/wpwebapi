# -*- coding: utf-8 -*-
import urllib.parse
import urllib.request
import json
import random
import csv
from model import WPSenderModel

class WPSender():
	def __init__(self, user):
		self.user = user
		self.messages_file = "inputs/messages.csv"
		self.numbers_file = "inputs/numbers.csv"

		self.numbers = self._get_numbers()
		self.model = WPSenderModel()

	def _get_numbers(self):
		numbers = []		
		with open(self.numbers_file, encoding='utf-8') as csvfile:
			csvreader = csv.reader(csvfile)

			for row in csvreader:
				numbers = [x for x in row if x]

		self.numbers = numbers		
		return numbers

	def _get_message(self):	
		messages = []
		with open(self.messages_file) as csvfile:
			csvreader = csv.reader(csvfile)

			for row in csvreader:
				messages = [x for x in row if x]

		#return str(random.choice(messages))
		return "fala comigo não?"


	def _save_envio(self):
		print("> Registrando envio em banco de dados...")
		for number in self.numbers:
			self.model.add(number)

		print("Envio registrado")

	def _send_message(self, number, counter):
		id = number + "@c.us"
		print(">> %d) Enviando para %s..." % (counter, number))
		
		uri = "http://127.0.0.1:5000/send"
		query = {
			"user": self.user,
			"number": number,
			"message": self._get_message()
		}
		uri = "%s?%s" % (uri, urllib.parse.urlencode(query))
		with urllib.request.urlopen(uri) as response:
			data = json.loads(response.read())

		if data["status"] == 5:
			print(">>>>> %d) Enviado com sucesso para %s" % (counter, number))
			self.model.change_status(number)
		elif data["status"] == 6:
			print(">>>>> %d) Impossível enviar para %s" % (counter, number))
		elif data["status"] == 7:
			print(">>>>> %d) Falta mensagem u destinatário" % counter)


	def login(self):
		res = {}
		uri = "http://127.0.0.1:5000/start_login?user=%s" % self.user
		with urllib.request.urlopen(uri) as response:
			data = json.loads(response.read())

		if data["status"] == 1:
			print ("> Leia o QRCode em %s" % data["qr_code"])
			res["status"] = 1
		if data["status"] == 2:
			print ("> Whatsapp logado")
			res["status"] = 2

		return res

	def check_login(self):
		res = {}
		uri = "http://127.0.0.1:5000/check_login?user=%s" % self.user
		with urllib.request.urlopen(uri) as response:
			data = json.loads(response.read())

		if data["status"] == 4:
			print ("> Login não iniciado")
			res["status"] = 4
		if data["status"] == 2:
			print ("> Whatsapp logado")
			res["status"] = 2
		if data["status"] == 3:
			print("> Whatsapp não logado")
			res["status"] = 3

		return res

	def start(self):
		self._save_envio()
		print ("> Iniciando envio para %s números" % len(self.numbers))
		i = 0
		for number in self.numbers:
			obj = self.model.get(number)
			if obj and obj["status"] == 0:
				i += 1
				self._send_message(number, i)

		print("====== Concluido envio para %s numeros" % len(self.numbers))




